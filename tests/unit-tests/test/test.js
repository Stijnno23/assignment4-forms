// Load configuration
var env = process.env.NODE_ENV || 'development',
    config = require('../../../server/config/config.js')[env],
    localConfig = require('../config-test.json')
    ;

var should = require('should'),
    assert = require('assert'),
    supertest = require('supertest');

describe('API Routing for CRUD operations on users', function () {
    var request = supertest(localConfig.host + ":" + config.port);

    var tempuserId = null;


    before(function (done) {
        //mongoose.connect(testdata.db.mongodb);
        done();
    });

    describe('CREATE user', function () {
        it('Should POST /users', function (done) {
            request
                .post('/users')
                .send({
                    "email": "John.Doe@example.com",
                    "password": "12345678",
                    "confirmPassword": "12345678"
                })
                .expect('Content-Type', /application.json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    JSON.parse(res.text).should.have.property('meta').and.have.property('action').be.exactly('create');
                    JSON.parse(res.text).should.have.property('err').be.exactly(null);
                    res.should.have.status(200);
                    tempuserId = JSON.parse(res.text).doc._id;
                    done();
                });
        });
    });


    describe('RETRIEVE all users', function () {
        it('Should GET /users', function (done) {
            request
                .get('/users')
                .expect('Content-Type', /application.json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    JSON.parse(res.text).should.have.property('meta').and.have.property('action').be.exactly('list');
                    res.should.have.status(200);
                    done();
                });
        });
    });

    describe('RETRIEVE 1 user', function () {
        it('Should GET /users/{id}', function (done) {
            request
                .get('/users/' + tempuserId)
                .expect('Content-Type', /application.json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    JSON.parse(res.text).should.have.property('meta').and.have.property('action').be.exactly('detail');
                    res.should.have.status(200);
                    done();
                });
        });
    });


    describe('UPDATE 1 user', function () {
        it('Should PUT /users/{id}', function (done) {
            request
                .put('/users/' + tempuserId)
                .send({
                    "email": "Jane.Doe@example.com",
                    "password": "12345678",
                    "confirmPassword": "12345678"
                })
                .expect('Content-Type', /application.json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    JSON.parse(res.text)
                        .should.have.property('meta')
                        .and.have.property('action').be.exactly('update');
                    JSON.parse(res.text)
                        .should.have.property('err').be.exactly(null);
                    res.should.have.status(200);
                    done();
                });
        });
    });


    describe('DELETE 1 user', function () {
        it('Should DELETE /users/{id}', function (done) {
            request
                .del('/users/' + tempuserId)
                .expect('Content-Type', /application.json/)
                .expect(200)
                .end(function (err, res) {
                    if (err) {
                        throw err;
                    }
                    JSON.parse(res.text).should.have.property('meta').and.have.property('action').be.exactly('delete');
                    JSON.parse(res.text).should.have.property('doc').be.exactly(1);
                    JSON.parse(res.text).should.have.property('err').be.exactly(null);
                    res.should.have.status(200);
                    done();
                });
        });
    });


});